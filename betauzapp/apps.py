from django.apps import AppConfig


class BetauzappConfig(AppConfig):
    name = 'betauzapp'
