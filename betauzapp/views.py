from django.shortcuts import render, redirect, reverse
from django.http import HttpResponse, HttpResponseRedirect, Http404 
from django.db.models import Q
from django.contrib.auth.models import User

from .models import News 
from .forms import RegisterForm
# Create your views here.


def home(request):
    news = News.objects.order_by('-date_published')[:3]
    popular = News.objects.filter(views__gte=10)
    return render(request, 'betauzapp/index.html', {'news': news, 'popular': popular})


def blog(request):
    yangliklar = News.objects.all()
    return render(request, 'betauzapp/index.html', {'yangliklar': yangliklar})


def search_result(request):
    query = request.GET.get('search')
    search_obj = News.objects.filter(
        Q(title__icontains=query) | Q(description__icontains=query)
    )
    return render(request, 'betauzapp/search.html', {'search_obj': search_obj, 'query': query})


def news_detail(request, slug):
    news = News.objects.get(slug__iexact=slug)
    news.views += 1
    news.save()
    return render(request, 'betauzapp/news_detail.html', context={'news': news})


def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            # log the user in
            return redirect('home')
    else:
        form = RegisterForm()
    return render(request, 'betauzapp/register.html', {'form': form})


def leave_comment(request, slug):
    try:
        news = News.objects.get(slug__iexact=slug)
    except:
        raise Http404("maqola topilmad")
    if request.user.is_authenticated:
        user = request.user.first_name
        news.comments.create(author_name=user,comment_text=request.POST.get('comment_text'))
    else:
        news.comments.create(author_name=request.POST.get('name'), comment_text=request.POST.get('comment_text'))
    return HttpResponseRedirect(reverse('news_detail_url', args=(news.slug,)))