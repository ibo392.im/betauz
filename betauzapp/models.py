from django.db import models

from django.utils import timezone
from django.shortcuts import reverse
# Create your models here.


class Category(models.Model):
    title = models.CharField('sarlovha', max_length=150)
    slug = models.SlugField('Manzil', max_length=150, unique=True)

    class Meta:
        verbose_name = 'Kategoriya'
        verbose_name_plural = 'Kategoriyalar'

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('Category_detail_url', kwargs={'slug': self.slug})


class News(models.Model):
    title = models.CharField('sarlovha', max_length=255)
    slug = models.SlugField('Manzil', max_length=150, unique=True)
    description = models.TextField('Izoh', blank=True, db_index=True)
    image = models.ImageField('Rasm', db_index=True)
    date_published = models.DateTimeField('Sana', default=timezone.now)
    categories = models.ForeignKey(Category, on_delete=models.CASCADE)
    views = models.IntegerField(default=0)

    class Meta:
        verbose_name = 'Yangilik'
        verbose_name_plural = 'Yangiliklar'

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('news_detail_url', kwargs={'slug': self.slug})



class Comment(models.Model):
    news = models.ForeignKey(News, on_delete=models.CASCADE, related_name='comments' )
    author_name = models.CharField('Foydalanuvchi', max_length=50)
    comment_text = models.TextField('Komintariya', max_length=1000)


    class Meta:
        verbose_name = 'Komentariya'
        verbose_name_plural = 'Komentariyalar'


    def __str__(self):
        return self.author_name


















