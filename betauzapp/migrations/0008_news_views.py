# Generated by Django 3.1.1 on 2020-12-15 15:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('betauzapp', '0007_auto_20201208_2109'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='views',
            field=models.IntegerField(default=0),
        ),
    ]
