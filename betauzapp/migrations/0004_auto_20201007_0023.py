# Generated by Django 3.1.1 on 2020-10-06 19:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('betauzapp', '0003_auto_20201002_1904'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=150, verbose_name='sarlovha')),
                ('slug', models.SlugField(max_length=150, unique=True, verbose_name='Manzil')),
            ],
            options={
                'verbose_name': 'Kategoriya',
                'verbose_name_plural': 'Kategoriyalar',
            },
        ),
        migrations.AlterField(
            model_name='news',
            name='slug',
            field=models.SlugField(max_length=150, unique=True, verbose_name='Manzil'),
        ),
    ]
